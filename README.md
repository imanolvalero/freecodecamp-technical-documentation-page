# LS Man Page

Copy of `ls` commnad man page from [die.net](https://linux.die.net/man/1/ls) with custom style.

[Try it here](https://imanolvalero.gitlab.io/freecodecamp-technical-documentation-page/)

## Motivation

Project [__Technical Documentation Page__](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-projects/build-a-technical-documentation-page)
 of the course __[Responsive Web Design Certification](https://www.freecodecamp.org/learn/responsive-web-design/)__ taught by __[freeCodeCamp.org](https://www.freecodecamp.org/)__.
